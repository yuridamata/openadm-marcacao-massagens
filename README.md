# Aviso

O presente projeto tem objetivo **didático**.

As descrições aqui presentes não se propõem a modelar ou refletir qualquer prestação de serviço real.

Os mecanismos de segurança da informação aqui propostos não tem o compromisso de satisfazer quaisquer legislações referentes à proteção de dados, segurança da informação ou outras análogas.

Assim, não nos responsabilizamos por qualquer uso deste software, tampouco por eventuais relações de prestação de serviço que venham a utilizá-lo para conduzir suas atividades.

## Marcação Massagens

Este sistema tem como objetivo controlar a prestação de serviço de massagista no seguinte contexto:

- Uma grande empresa disponibilizou para seus funcionários orçamento para que fosse contratado o serviço de massagista.
- O pagamento do serviço é feito por cada sessão de massagem.
- Funcionários que usurfruírem do serviço de massagem devem confirmar que a sessão foi realizada corretamente.
- Cada profissional prestador de serviços deve ter seu cadastro realizado no sistema.
- O sistema deve ser gerar um relatório de sessões realizadas para que o pagamento dos serviços sejam realizados.

## Tecnologias utilizadas

- Typescript
- Javascript
- NodeJS
- ReactJs
- NextJS
- NestJs
- Ant Design
